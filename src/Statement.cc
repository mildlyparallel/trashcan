#include <cassert>
#include <cstring>
#include <thread>

#include "Statement.hh"
#include "Database.hh"
#include "Utils.hh"
#include "Logger.hh"
#include "UserError.hh"

#include <sqlite3.h>

#define CHECK(rc) {\
	if ((rc) != SQLITE_OK) { \
		logger.error("sqlite3 error #", sqlite3_errcode(m_handle), "in", __func__, "@ line", __LINE__, ":", sqlite3_errmsg(m_handle)); \
		throw SqliteException(sqlite3_errcode(m_handle)); \
	} \
} while(false);

static const char *col_type_str(int t)
{
	switch (t) {
		case SQLITE_NULL:
			return "NULL";
		case SQLITE_INTEGER:
			return "INTEGER";
		case SQLITE_TEXT:
			return "TEXT";
		case SQLITE_FLOAT:
			return "FLOAT";
		case SQLITE_BLOB:
			return "BLOB";
		default:
			return "Unknown";
	}
}

Statement::Statement(Database &db, const char *sql)
: m_stmt(nullptr)
, m_handle(db.get_handle())
{ 
#ifndef NDEBUG
	logger.debug("Prepearing", sql);
#endif

	
	do {
		int rc = sqlite3_prepare_v2(m_handle, sql, -1, &m_stmt, nullptr);

		if (rc == SQLITE_LOCKED) {
			rc = sqlite3_reset(m_stmt);
			CHECK(rc);
		}

		if (rc == SQLITE_BUSY || rc == SQLITE_LOCKED) {
			logger.debug("sqlite3_prepare_v2(): database is busy or locked, retrying");
			std::this_thread::sleep_for(std::chrono::milliseconds(busy_retry_delay_ms));
			continue;
		}

		CHECK(rc);
		break;
	} while (true);
}

Statement::Statement(Statement &&other)
: m_stmt(other.m_stmt)
, m_handle(other.m_handle)
{
	other.m_stmt = nullptr;
	other.m_handle = nullptr;
}

void Statement::operator= (Statement &&other)
{
	m_stmt = other.m_stmt;
	m_handle = other.m_handle;
	other.m_stmt = nullptr;
	other.m_handle = nullptr;
}

Statement::~Statement()
{
	sqlite3_finalize(m_stmt);
}

void Statement::bind_to(size_t pos, const std::string_view &value)
{
	logger.debug("Binding", pos, "string_view:", value.substr(0, utils::min(value.size(), 16u)));

	int rc = sqlite3_bind_text(m_stmt, pos+1, value.data(), value.size(), nullptr);
	CHECK(rc);
}

void Statement::bind_to(size_t pos, const std::string &value)
{
	logger.debug("Binding", pos, "string:", value.substr(0, utils::min(value.size(), 16u)));

	int rc = sqlite3_bind_text(m_stmt, pos+1, value.data(), value.size(), nullptr);
	CHECK(rc);
}

void Statement::bind_to(size_t pos, const char *value)
{
	assert(value);

	logger.debug("Binding", pos, "cstring:", value);

	int rc = sqlite3_bind_text(m_stmt, pos+1, value, std::strlen(value), nullptr);
	CHECK(rc);
}

void Statement::bind_to(size_t pos, uint64_t value)
{
	logger.debug("Binding", pos, "uint64_t:", value);

	int rc = sqlite3_bind_int64(m_stmt, pos+1, value);
	CHECK(rc);
}

bool Statement::step()
{
	do {
		int rc = sqlite3_step(m_stmt);
		if (rc == SQLITE_DONE || rc == SQLITE_OK)
			return false;
		if (rc == SQLITE_ROW)
			return true;

		logger.debug("sqlite3_step() @ line", __LINE__, ":",  sqlite3_errmsg(m_handle));

		if (rc == SQLITE_CONSTRAINT)
			throw UserError(UserError::InvalidValue);

		if (rc == SQLITE_LOCKED) {
			rc = sqlite3_reset(m_stmt);
			CHECK(rc);
		}

		if (rc == SQLITE_BUSY || rc == SQLITE_LOCKED) {
			logger.debug("sqlite3_step(): database is busy or locked, retrying");
			std::this_thread::sleep_for(std::chrono::milliseconds(busy_retry_delay_ms));
			continue;
		}

		logger.error("sqlite3 error @ line", __LINE__, ":",  sqlite3_errmsg(m_handle));
		throw SqliteException(rc);
	} while (true);
}

size_t Statement::nr_columns() const
{
	return sqlite3_column_count(m_stmt);
}

bool Statement::load()
{
	return step();
}

const char *Statement::name_at(size_t num) const
{
	return sqlite3_column_name(m_stmt, num);
}

const char *Statement::type_at(size_t pos) const
{
	int coltype = sqlite3_column_type(m_stmt, pos);
	return col_type_str(coltype);
}

void Statement::load_at(size_t pos, uint64_t &value)
{
	logger.debug("Loading", pos, type_at(pos), name_at(pos), "to utin64_t");

	int coltype = sqlite3_column_type(m_stmt, pos);

	if (coltype ==  SQLITE_NULL) {
		value = 0;
		return;
	}

	assert(coltype == SQLITE_INTEGER);

	value = sqlite3_column_int64(m_stmt, pos);
}

void Statement::load_at(size_t pos, std::string &value)
{
	logger.debug("Loading", pos, type_at(pos), name_at(pos), "to string");

	int coltype = sqlite3_column_type(m_stmt, pos);

	if (coltype ==  SQLITE_NULL) {
		value.clear();
		return;
	}

	assert(coltype == SQLITE_TEXT);

	const unsigned char *s = sqlite3_column_text(m_stmt, pos);
	value = std::string(reinterpret_cast<const char*>(s));
}

void Statement::load_at(size_t pos, double &value)
{
	logger.debug("Loading", pos, type_at(pos), name_at(pos), "to double");

	int coltype = sqlite3_column_type(m_stmt, pos);

	if (coltype == SQLITE_NULL) {
		value = 0;
		return;
	}

	assert(coltype == SQLITE_FLOAT);

	value = sqlite3_column_double(m_stmt, pos);
}
