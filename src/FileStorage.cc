#include <cassert>

#include "FileStorage.hh"
#include "Logger.hh"
#include "Config.hh"

namespace fs = std::filesystem;

FileStorage::FileStorage(const Config &config)
: m_storage_dir(config.storage_dir)
{  }

fs::path FileStorage::get_path(uint64_t id) const
{
	return m_storage_dir / std::to_string(id);
}

bool FileStorage::exists(uint64_t id) const
{
	return fs::exists(get_path(id));
}

void FileStorage::add(uint64_t id, const std::string_view &buffer)
{
	assert(!buffer.empty());

	auto p = get_path(id);

	logger.debug("Writing", buffer.size(), "bytes to", p);

	std::ofstream out(p, std::ostream::out | std::ostream::binary);
	out.write(buffer.data(), buffer.size());
}

bool FileStorage::remove(uint64_t id)
{
	auto p = get_path(id);

	logger.debug("Deleting", p);

	bool ok = fs::remove(p);
	if (!ok)
		logger.debug("Failed to delete", p);

	return ok;
}

