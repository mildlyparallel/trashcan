#include <cassert>
#include <iostream>

#include "Request.hh"
#include "Config.hh"
#include "Logger.hh"

extern "C" {
#include <mongoose.h>
}

Request::Request(mg_connection *con, mg_http_message *req)
: m_connection(con)
, m_message(req)
{ 
	assert(m_connection);
	assert(m_message);

	get_method_value();
}

mg_connection *Request::get_connection() const
{
	return m_connection;
}

mg_http_message *Request::get_message() const
{
	return m_message;
}

std::string_view Request::method_str() const
{
	return std::string_view(m_message->method.ptr, m_message->method.len);
}

Request::Method Request::method() const
{
	return m_method;
}

std::string_view Request::uri() const
{
	return std::string_view(m_message->uri.ptr, m_message->uri.len);
}

std::string_view Request::body() const
{
	return std::string_view(m_message->body.ptr, m_message->body.len);
}

std::string_view Request::query() const
{
	return std::string_view(m_message->query.ptr, m_message->query.len);
}

bool Request::match_uri(const char *uri) const
{
	return mg_http_match_uri(m_message, uri);
}

void Request::get_method_value()
{
	auto m = method_str();

	for (size_t i = 0; i < nr_methods; ++i) {
		if (m != methods_strings[i])
			continue;

		m_method = static_cast<Method>(i);
		return;
	}

	logger.warning("Unsupported method requested:", m);
	m_method = Method::Get;
	assert(false);
}

std::optional<std::string> Request::get_query_var(const char *name) const
{
	char buf[Config::url_param_buf_size] = {0};

	int rc = mg_http_get_var(&m_message->query, name, buf, sizeof(buf) - 1);
	if (rc > 0)
		return std::string(buf, rc);

	return {};
}

uint64_t Request::get_query_ull(const char *name) const
{
	try {
		std::optional<std::string> s = get_query_var(name);
		if (s)
			return std::stoull(s.value());
	} catch (...) {  };

	return 0ull;
}

std::string_view Request::get_uri_var(size_t pos_from_end) const
{
	const char *cur = m_message->uri.ptr + m_message->uri.len;

	size_t pos = 0;
	size_t len = 0;

	while (cur >= m_message->uri.ptr) {
		if (*cur == '/') {
			if (pos == pos_from_end)
				return std::string_view(cur + 1, len - 1);

			len = 0;
			pos++;
		}

		len++;
		cur--;
	}

	return {};
}

size_t Request::get_next_part(
	size_t offset,
	std::string_view &name,
	std::string_view &filename,
	std::string_view &body
) const {
	struct mg_http_part part;

	offset = mg_http_next_multipart(m_message->body, offset, &part);

	if (offset > 0) {
		name = std::string_view(part.name.ptr, part.name.len);
		filename = std::string_view(part.filename.ptr, part.filename.len);
		body = std::string_view(part.body.ptr, part.body.len);
	}

	return offset;
}

void Request::reply(int code, const std::string &headers, const std::string &body) const
{
	logger.verbose("Replying", code, "size", body.size());

	logger.debug("headers", headers);
	mg_http_reply(m_connection, code, headers.c_str(), "%s", body.c_str());
}

void Request::reply(const std::string &body) const
{
	logger.verbose("Replying 200, size", body.size());

	static const char *headers = TC_DEFAULT_HEADERS TC_TC_CORS_HEADERS;
	mg_http_reply(m_connection, 200, headers, "%s", body.c_str());
}

void Request::reply() const
{
	logger.verbose("Replying 200, (no body)");

	static const char *headers = TC_DEFAULT_HEADERS TC_TC_CORS_HEADERS;
	mg_http_reply(m_connection, 200, headers, "");
}

void Request::reply_error(int code, const char *msg) const
{
	logger.verbose("Replying", code);

	static const char *headers = TC_DEFAULT_HEADERS TC_TC_CORS_HEADERS;
	mg_http_reply(m_connection, code, headers, "{\"error\": \"%s\"}\n", msg);
}

void Request::serve_dir(const std::string &root_dir) const
{
	logger.verbose("Replying from dir", root_dir);

	struct mg_http_serve_opts opts = {};
	opts.root_dir = root_dir.c_str();

	mg_http_serve_dir(m_connection, m_message, &opts);
}

void Request::serve_file(
	const std::filesystem::path &file,
	const std::string &mime,
	const std::string &hdrs
) const {
	logger.verbose("Replying with file", file, "mime", mime);

	mg_http_serve_file(m_connection, m_message, file.c_str(), mime.c_str(), hdrs.c_str());
}

std::tuple<std::string_view, std::string_view> Request::get_first_file() const
{
	for (const auto &[name, file, body] : *this) {
		if (!file.empty() && !body.empty())
			return {file, body};
	}

	return {};
}

Request::Iterator::Iterator(mg_http_message *msg, size_t offset)
: m_message(msg)
, m_offset(offset)
{ }

Request::Iterator &Request::Iterator::operator++() {
	if (m_offset == static_cast<size_t>(-1))
		return *this;

	struct mg_http_part part;

	m_offset = mg_http_next_multipart(m_message->body, m_offset, &part);

	if (m_offset > 0) {
		std::get<0>(m_value) = std::string_view(part.name.ptr, part.name.len);
		std::get<1>(m_value) = std::string_view(part.filename.ptr, part.filename.len);
		std::get<2>(m_value) = std::string_view(part.body.ptr, part.body.len);

		logger.debug(
			"Chunk name:", std::get<0>(m_value),
			"file:", std::get<1>(m_value).size(),
			"size:", std::get<2>(m_value).size()
		);
	} else {
		m_offset = static_cast<size_t>(-1);
	}

	return *this;
}

Request::Iterator::reference Request::Iterator::operator*()
{
	return m_value;
}

bool operator== (const Request::Iterator& a, const Request::Iterator& b)
{
	return a.m_offset == b.m_offset;
}

bool operator!= (const Request::Iterator& a, const Request::Iterator& b)
{
	return a.m_offset != b.m_offset;
}

Request::Iterator Request::begin() const
{
	return Request::Iterator(m_message, 0);
}

Request::Iterator Request::end() const
{
	return Request::Iterator(m_message, -1);
}

std::ostream &operator<< (std::ostream &os, Request::Method method)
{
	size_t i = static_cast<size_t>(method);
	os << Request::methods_strings[i];
	return os;
}
