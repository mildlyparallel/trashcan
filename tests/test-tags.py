import os
import unittest
import requests
import urllib
import random
import string

from resttest import RestTest

class TestTags(RestTest):
    def test__get__empty__returns_list(self):
        (code, body) = self._get("tags")
        self.assertEqual(code, 200);
        self.assertTrue(type(body) == list);

    def test__post__all_fields__returns_id(self):
        (code, body) = self._post("/tags", [
            ('name', 'name1'),
            ('color', '333')
        ])

        self.assertEqual(code, 200);
        self.assertIn('tag_id', body);
        self.assertTrue(body['tag_id'] > 0);

    def test__post__no_color__returns_id(self):
        (code, body) = self._post("/tags", [
            ('name', 'name2')
        ])

        self.assertEqual(code, 200);
        self.assertIn('tag_id', body);
        self.assertTrue(body['tag_id'] > 0);

    def test__post__empty_name__returns_err(self):
        (code, body) = self._post("/tags", [ ('name', '') ])
        self.assertEqual(code, 400);

    def test__post__no_name__returns_err(self):
        (code, body) = self._post("/tags", [ ('k', 'v') ])
        self.assertEqual(code, 400);

    def test__post_get(self):
        tag = {
            'name': self._genstr(),
            'color': self._genstr()
        }

        (code, body) = self._post("/tags", tag)
        self.assertEqual(code, 200);

        tag['tag_id'] = body['tag_id']

        (code, body) = self._get('tags')
        self.assertEqual(code, 200);
        self.assertIn(tag, body);

    def test__post_delete_get__tag_deleted(self):
        tag = {
            'name': self._genstr(),
            'color': self._genstr()
        }

        (code, body) = self._post("/tags", tag)
        self.assertEqual(code, 200);

        tag['tag_id'] = body['tag_id']

        (code, body) = self._delete(f"tags/{tag['tag_id']}")
        self.assertEqual(code, 200);

        (code, body) = self._get('tags')
        self.assertEqual(code, 200);
        self.assertNotIn(tag, body);

if __name__ == '__main__':
    unittest.main()

