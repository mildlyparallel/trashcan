#!/bin/bash

set -ex

url=${1:-"http://localhost:8000"}

# curl -F "file=@/etc/fstab"  -F "Title=aa ыы ййол" -F "Year=1990" -F "Author=author1" -F "Doi=123/3245" $url/papers 
# # curl -F "file=@/etc/passwd" -F "Title=title2" -F "Year=1990" -F "Author=author1" -F "Doi=123/3245" $url/papers 
# curl -F "file=@/etc/fstab"  -F "Title=title1" -F "Tags=1,2,3" $url/papers 

function run() {
	curl $@ 2>/dev/null | jq
}

run -F "name=dolphins" -F "color=#AA2299" $url/tags 
run -F "name=bottles" -F "color=#00FFAA" $url/tags 
run -F "name=leafs" -F "color=#32FFAA" $url/tags 
run -F "name=engines" -F "color=#EE1100" $url/tags 
run -F "name=glue" -F "color=#DEADDE" $url/tags 

run $url/tags

run -X DELETE $url/tags/4

run -F "title=title1" -F "first_author=author1" -F "year=year1" -F "doi=123/123" -F "tags=1" -F "tags=3" -F "file=@/etc/fstab" $url/papers 
run -F "title=title2" -F "first_author=author2" -F "year=year2" -F "doi=456/123" -F "tags=1" -F "tags=2" -F "file=@/etc/passwd" $url/papers 
run -F "title=title3" -F "tags=1" -F "tags=3" $url/papers 

run $url/papers/1

run -X DELETE $url/papers/3

run $url/papers

run -F "title=Bepis" -F "year=1971" $url/papers/1
run -F "file=@/etc/hosts" $url/papers/1/file
run -F "tags=1" -F "tags=2" $url/papers/1/tags
run -F a=1 $url/papers/1/index
run -X DELETE $url/link/5
run -X DELETE $url/link/1
run -X DELETE $url/link/3

run -F a=1 $url/papers/query
run -F "tags=2" $url/papers/query
run -F "query=benis" $url/papers/query
run -F "tags=2" -F "query=benis" $url/papers/query

