#!/bin/bash

scripts_root="$(dirname $0)"
env_file="$scripts_root/trash.env"

[ -r "$env_file" ] && source "$env_file"

tc_url="${TC_URL:-"http://localhost:8000/"}"
tmpdir="${TC_TMPDIR:-"/tmp/"}"
scihub="${TC_SCIHUB:-"https://sci-hub.do/"}"

function usage() {
	echo $0 [-y] [-d doi] [-f file] [-T title] [-A author] [-Y year]
}

doi=
filesource=
filepath=
title=
author=
year=
bibtex=
confirm=false

while getopts "h?yd:f:T:A:Y:" opt
do
	case "$opt" in
		h|\?)
			usage
			exit 0
			;;
		y)
			confirm=true
			;;
		d)
			doi=$OPTARG
			;;
		f)
			filesource=$OPTARG
			;;
		T)
			title=$OPTARG
			;;
		A)
			author=$OPTARG
			;;
		Y)
			year=$OPTARG
			;;
	esac
done

function doi_to_url() {
	if [[ "$1" != https://doi.org/* ]]
	then
		echo "https://doi.org/$1"
	else
		echo "$1"
	fi
}

function fetch_metadata() {
	doi="$(doi_to_url "$doi")"

	metadata="$tmpdir/trashcan-metadata.json"

	echo Fetching metadata for "$doi"

	> "$metadata"
	curl --silent -LH "Accept: application/json" "$doi" -o "$metadata"

	mtype="$(file --brief "$metadata")"
	if [ "$mtype" != "JSON data" ] 
	then
		echo "Returned metadata is not JSON (got $mtype)"
		return
	fi

	doi_data="$(cat "$metadata")"
	title="$(echo "$doi_data" | jq '.title')"
	author=$(echo "$doi_data" | jq '.author[0].family + " " + .author[0].given')
	authors="$(echo "$doi_data" | jq '[.author[] | .family + " " + .given] | join(", ")')"
	year="$(echo "$doi_data" | jq '.created["date-parts"][0][0]')"

	echo Fetching bibtex for "$doi"

	bibout="$tmpdir/trashcan-bibtex"
	> "$bibout"
	curl --silent -LH "Accept: text/bibliography; style=bibtex" "$doi" -o "$bibout"
	bibtex="$(cat "$bibout")"
}

function get_dowload_url {
	curl --silent "${scihub}${doi}" \
		| grep -o 'location.href=.*pdf' \
		| sed "s:location.href='::" \
		| sed "s:^//::"
}

function fetch_file_from_url() {
	local url="$1"

	echo Fetching PDF from "$url"

	output="$tmpdir/trashcan-file.pdf"
	> $output
	curl --silent -L "$url" -o "$output"

	t="$(file --brief "$output")"
	if [[ "$t" != PDF* ]]
	then
		echo Failed to fetch PDF, got $t
		return
	fi

	filepath="$output"
	echo Fetched PDF to "$filepath"
}

function fetch_file_from_doi() {
	url="$(get_dowload_url)"

	if [ -z "$url" ]
	then
		echo Failed to find PDF download link
		return
	fi

	fetch_file_from_url "$url"
}

if [ -r "$filesource" ]
then
	filepath="$filesource"
elif [[ "$filesource" == http* ]]
then
	fetch_file_from_url "$filesource"
fi

if [ -n "$doi" ]
then
	fetch_metadata

	[ ! -n "$filesource" ] && fetch_file_from_doi 
fi

echo
echo About to submit:
echo "  DOI: $doi"
echo "  Title: $title"
echo "  Author: $author"
echo "  Authors: $authors"
echo "  Year: $year"
echo "  File: $filepath"
echo "  Bibtex: $bibtex"

if [ ! -r "$filepath" ] && [ -z "$title" ]
then
	echo "file or title are not defined, exiting"
	exit 1
fi

if ! $confirm
then
	echo
	while true; do
		read -p "Sumbit this paper (Y/N)?: " yn
		case $yn in
			[Yy]*)
				break
				;;
			*)
				exit 1
				;;
		esac
	done
fi


req=
[ -n "$filepath" ] && [ -r $filepath ] && req="$req -F "file=@$filepath""

curl \
	-F "title=$title" \
	-F "doi=$doi" \
	-F "year=$year" \
	-F "first_author=$author" \
	-F "authors=$authors" \
	-F "bibtex=$bibtex" \
	$req ${tc_url}papers | jq

