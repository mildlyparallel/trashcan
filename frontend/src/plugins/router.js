import Vue from 'vue';
import VueRouter from 'vue-router';

import PapersList from '../components/PapersList';
import TagsEditPage from '../components/TagsEditPage';
import SettingsPage from '../components/SettingsPage';
import PaperEdit from '../components/PaperEdit';
import PaperAdd from '../components/PaperAdd';

Vue.use(VueRouter);

const routes = [
	{
		path: '',
		component: PapersList,
		meta: { title: 'Trash' }
	},
	{
		path: '/tags',
		component: TagsEditPage,
		meta: { title: 'Tags | Trash' }
	},
	{
		path: '/settings',
		component: SettingsPage,
		meta: { title: 'Settings | Trash' }
	},
	{
		path: '/add',
		component: PaperAdd,
		meta: { title: 'New Paper | Trash' }
	},
	{
		path: '/papers/:paper_id',
		component: PaperEdit,
		meta: { title: 'Edit Paper | Trash' }
	},
];

export default new VueRouter({ routes });

