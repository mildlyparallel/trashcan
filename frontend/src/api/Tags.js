import {Backend} from '../api/Backend';

const Tags = {
	add: async function(name, color) {
		var obj = await Backend.post('tags', [{name: name}, {color: color}]);
		return obj.tag_id;
	},

	find: async function() {
		return await Backend.get('tags');
	},

	remove: async function(tag_id) {
		await Backend.delete(`tags/${tag_id}`);
	},
};

export { Tags };

