const monthNames = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
];

const maxKeyTokenLength = 16;

const Bibtex = {
	getKey: function(data) {
		if (!data.year || !data.title || !data.authors)
			return null;

		if (!data.authors[0] || !data.authors[0].surname)
			return null;

		var author = data.authors[0].surname;

		var key = '';

		var sp1 = author.indexOf(' ');
		if (sp1 < 0 || sp1 > maxKeyTokenLength)
			sp1 = maxKeyTokenLength;
		key += author.substr(0, sp1)

		key += data.year;

		var sp2 = data.title.indexOf(' ');
		if (sp2 < 0 || sp2 > maxKeyTokenLength)
			sp2 = 16;
		key += data.title.substr(0, sp2)

		key = key.toLowerCase();

		return key;
	},

	getStr: function(type, data) {
		var key = this.getKey(data);

		if (!key)
			return null;

		var month = null;
		if (data.month >= 0 && data.month < 12)
			month = monthNames[data.month];

		var fields = {
			title: data.title,
			author: (data.authors.map((v) => {
				if (v.surname && v.firstname)
					return v.surname + ', ' + v.firstname
				else
					return v.name;
			})).join(' and '),
			year: data.year,
			month: month,
			publisher: data.publisher,
			journal: data.journal,
			DOI: data.doi,
			pages: data.pageCount
		};

		var str = '@' + type + '{' + key;

		var added = false;
		for (let k in fields) {
			if (!fields[k])
				continue;
			str += `, ${k} = {${fields[k]}}`;
			added = true;
		}

		str += '}';

		if (!added)
			return null;

		return str;
	},
};

export { Bibtex };

