import axios from 'axios';

import {Bibtex} from './Bibtex';

const getArxivUrl = function(arxivId) {
	if (!arxivId)
		return null;

	var url = new URL('http://export.arxiv.org/api/query');
	url.searchParams.set('id_list', arxivId);

	if (!url.href)
		throw 'Failed to parse arxiv ' + arxivId;

	return url.href;
};

const fetchMetadataXml = async function(arxivId) {
	var url = getArxivUrl(arxivId);

	if (!url)
		return {};

	var res = await axios({
		method: "GET",
		url: url,
		headers: {
			"Accept": "application/json"
		}
	});

	if (res.status != 200)
		throw `GET ${url} returned ${res.status}`;

	return res.data;
};

const getTagValue = function(body, tag) {
	var el = body.getElementsByTagName(tag);

	if (el.length == 0)
		return null;

	return el[0].childNodes[0].nodeValue;
}

const parseMetadata = function(xmlstr) {
	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString(xmlstr, "text/xml");

	var entry_el = xmlDoc.getElementsByTagName("entry")[0];

	var title = getTagValue(entry_el, "title");

	var authors = [];
	for (var author_el of entry_el.getElementsByTagName("author")) {
		var name = getTagValue(author_el, "name");
		if (!name)
			continue;

		var fields = {
			name: name,
			// affiliation: getTagValue(author_el, "arxiv:affiliation")
		};

		var tokens = name.split(' ');

		if (tokens.length > 1) {
			fields.firstname = tokens[0];
			fields.surname = tokens.slice(1).join(' ');
			fields.name = fields.surname + ' ' + fields.firstname;
		}

		authors.push(fields);
	}

	var published_str = getTagValue(entry_el, "published");

	var published = new Date(published_str);

	var url = getTagValue(entry_el, "id");

	var data = {
		title: title,
		url: url,
		year: published.getFullYear(),
		authors: (authors.map((v) => v.name)).join(', ')
	};

	var doi = getTagValue(entry_el, "arxiv:doi");

	var journal = getTagValue(entry_el, "arxiv:journal_ref");

	data.bibtex = Bibtex.getStr('article', {
		title: title,
		authors: authors,
		year: published.getFullYear(),
		month: published.getMonth(),
		doi: doi,
		journal: journal
	});

	return data;
};

const Arxiv = {
	fetch: async function(arxivstr) {
		var arxivId = arxivstr;
		if (arxivId.startsWith('arXiv:') || arxivId.startsWith('arxiv:'))
			arxivId = arxivId.substr(6);

		var xmlstr = await fetchMetadataXml(arxivId);

		var metadata = parseMetadata(xmlstr);

		metadata.arvix = arxivId;

		return metadata;
	},
};

export { Arxiv };

