#pragma once

#include <cassert>
#include <utility>
#include <cstddef>
#include <iostream>

#include "Statement.hh"
#include "Serializer.hh"
#include "UserError.hh"

class PapersIndex
{
public:
	struct Create : public Statement {
		static const constexpr char *sql =
			"CREATE VIRTUAL TABLE IF NOT EXISTS PapersIndex "
			"USING fts4( "
				"paper_id REFERENCES Papers(paper_id), "
				"title, "
				"authors, "
				"year, "
				"doi, "
				"isbn, "
				"arxiv, "
				"notes, "
				"content "
			"); ";

		Create(Database &db)
		: Statement(db, sql)
		{ }
	};

	struct Fields : public Statement {
		Fields(Database &db, const char *sql)
		: Statement(db, sql)
		{ }

		uint64_t paper_id;
		std::string_view title;
		std::string_view authors;
		std::string_view year;
		std::string_view doi;
		std::string_view isbn;
		std::string_view arxiv;
		std::string_view bibtex;
		std::string_view notes;
	};

	struct Insert : public Fields {
		static const constexpr char *sql =
			"INSERT INTO PapersIndex (paper_id, title, authors, year, doi, isbn, arxiv, notes) "
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

		Insert(Database &db)
		: Fields(db, sql)
		{ }

		inline void run()
		{
			assert(paper_id > 0);
			assert(!title.empty());

			bind_to(0, paper_id);
			bind_to(1, title);
			bind_to(2, authors);
			bind_to(3, year);
			bind_to(4, doi);
			bind_to(5, isbn);
			bind_to(6, arxiv);
			bind_to(7, notes);

			step();
		}
	};

	struct Update : public Fields {
		static const constexpr char *sql =
			"UPDATE PapersIndex SET "
				"title = ?, "
				"authors = ?, "
				"year = ?, "
				"doi = ?, "
				"isbn = ?, "
				"arxiv = ?, "
				"notes = ? "
			"WHERE paper_id = ?;";

		Update(Database &db)
		: Fields(db, sql)
		{ }

		inline void run()
		{
			assert(paper_id > 0);
			assert(!title.empty());

			bind_to(0, title);
			bind_to(1, authors);
			bind_to(2, year);
			bind_to(3, doi);
			bind_to(4, isbn);
			bind_to(5, arxiv);
			bind_to(6, notes);
			bind_to(7, paper_id);

			step();
		}
	};

	struct UpdateContent : public Statement {
		static const constexpr char *sql =
			"UPDATE PapersIndex SET content = ? WHERE paper_id = ?; ";

		UpdateContent(Database &db)
		: Statement(db, sql)
		{ }

		inline void run(uint64_t paper_id, const std::string &content)
		{
			bind_to(0, content);
			bind_to(1, paper_id);
			step();
		}
	};

	struct Delete : public Statement {
		static const constexpr char *sql =
			"DELETE FROM PapersIndex WHERE paper_id = ?;";

		Delete(Database &db)
		: Statement(db, sql)
		{ }

		uint64_t paper_id = 0;

		inline void run() {
			assert(paper_id > 0);
			bind_to(0, paper_id);
			step();
		}
	};

};

