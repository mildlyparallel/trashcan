#pragma once

#include <cassert>
#include <utility>
#include <cstddef>
#include <iostream>

#include "Statement.hh"
#include "Serializer.hh"
#include "UserError.hh"
#include "Papers.hh"

class PapersSelect : public Papers
{
public:
	struct Select : public Statement {
		Select(Database &db, const char *sql)
		: Statement(db, sql)
		{ }

		uint64_t paper_id;
		std::string title;
		std::string first_author;
		uint64_t year;
		uint64_t filesize;
		uint64_t created_at;

		virtual bool load() = 0;

		virtual void write(Serializer &s) = 0;
	};

	struct SelectOne : public Select {
		static const constexpr char *sql =
			"SELECT "
				"paper_id, "
				"title, "
				"first_author, "
				"authors, "
				"year, "
				"filename, "
				"filesize, "
				"filemime, "
				"created_at, "
				"updated_at, "
				"indexed_at, "
				"doi, "
				"isbn, "
				"bibtex, "
				"arxiv, "
				"url, "
				"notes "
			"FROM Papers "
			"WHERE Papers.paper_id = ?; ";

		SelectOne(Database &db)
		: Select(db, sql)
		{ }

		std::string authors;
		std::string filename;
		std::string filemime;
		uint64_t updated_at;
		uint64_t indexed_at;
		std::string doi, isbn, arxiv;
		std::string bibtex;
		std::string url;
		std::string notes;

		inline void bind(uint64_t paper_id)
		{
			bind_to(0, paper_id);
		}

		virtual bool load()
		{
			if (!step())
				return false;

			load_at(0, paper_id);
			load_at(1, title);
			load_at(2, first_author);
			load_at(3, authors);
			load_at(4, year);
			load_at(5, filename);
			load_at(6, filesize);
			load_at(7, filemime);
			load_at(8, created_at);
			load_at(9, updated_at);
			load_at(10, indexed_at);
			load_at(11, doi);
			load_at(12, isbn);
			load_at(13, bibtex);
			load_at(14, arxiv);
			load_at(15, url);
			load_at(16, notes);

			return true;
		}

		virtual void write(Serializer &s)
		{
			s.write_object(
				make_pair(PaperId, paper_id),
				make_pair(Title, title),
				make_pair(FirstAuthor, first_author),
				make_pair(Authors, authors),
				make_pair(Year, year),
				make_pair(Filename, filename),
				make_pair(Filesize, filesize),
				make_pair(Filemime, filemime),
				make_pair(CreatedAt, created_at),
				make_pair(UpdatedAt, updated_at),
				make_pair(IndexedAt, indexed_at),
				make_pair(Doi, doi),
				make_pair(Isbn, isbn),
				make_pair(Bibtex, bibtex),
				make_pair(Arxiv, arxiv),
				make_pair(Url, url),
				make_pair(Notes, notes)
			);
		}
	};

	struct SelectMultiple : public Select {
		SelectMultiple(Database &db, const char *sql)
		: Select(db, sql)
		{ }

		virtual bool load()
		{
			if (!step())
				return false;

			load_at(0, paper_id);
			load_at(1, title);
			load_at(2, first_author);
			load_at(3, year);
			load_at(4, filesize);
			load_at(5, created_at);

			return true;
		}

		virtual void write(Serializer &s)
		{
			s.write_object(
				make_pair(PaperId, paper_id),
				make_pair(Title, title),
				make_pair(FirstAuthor, first_author),
				make_pair(Year, year),
				make_pair(Filesize, filesize),
				make_pair(CreatedAt, created_at)
			);
		}
	};

	struct SelectAny : public SelectMultiple {
		static const constexpr char *sql =
			"SELECT paper_id, title, first_author, year, filesize, created_at "
			"FROM Papers "
			"ORDER BY paper_id DESC "
			"LIMIT ? OFFSET ?;";

		SelectAny(Database &db)
		: SelectMultiple(db, sql)
		{ }

		void bind(uint64_t limit, uint64_t offset) {
			bind_to(0, limit);
			bind_to(1, offset);
		}
	};

	struct SelectByTags : public SelectMultiple {
		static const constexpr char *sql_part_1 =
			"SELECT paper_id, title, first_author, year, filesize, created_at "
			"FROM Papers WHERE paper_id IN ( "
				"SELECT DISTINCT paper_id "
				"FROM TagLinks "
				"WHERE tag_id IN (";

		static const constexpr char *sql_part_2 = ") LIMIT ? OFFSET ?);";

		SelectByTags(Database &db, const char *sql)
		: SelectMultiple(db, sql)
		{ }

		void bind(uint64_t limit, uint64_t offset) {
			bind_to(0, limit);
			bind_to(1, offset);
		}
	};

	struct Match : public SelectMultiple {
		static const constexpr char *sql =
			"SELECT "
				"Papers.paper_id, "
				"Papers.title, "
				"Papers.first_author, "
				"Papers.year, "
				"Papers.filesize, "
				"Papers.created_at "
			"FROM PapersIndex  "
			"INNER JOIN Papers  "
			"ON "
				"Papers.paper_id = PapersIndex.paper_id "
				"AND PapersIndex MATCH ? "
				"LIMIT ? OFFSET ?";

		Match(Database &db)
		: SelectMultiple(db, sql)
		{ }

		void bind(const std::string_view &query, uint64_t limit, uint64_t offset) {
			bind_to(0, query);
			bind_to(1, limit);
			bind_to(2, offset);
		}
	};

	struct MatchTags : public SelectMultiple {
		static const constexpr char *sql_part_1 =
			"SELECT "
				"Papers.paper_id, "
				"Papers.title, "
				"Papers.first_author, "
				"Papers.year, "
				"Papers.filesize, "
				"Papers.created_at "
			"FROM Papers  "
			"INNER JOIN TagLinks "
			"ON "
				"TagLinks.paper_id = PapersIndex.paper_id "
				"AND TagLinks.tag_id IN (";

		static const constexpr char *sql_part_2 =
			") "
			"INNER JOIN PapersIndex "
			"ON "
				"Papers.paper_id = PapersIndex.paper_id "
				"AND PapersIndex MATCH ? "
			"LIMIT ? OFFSET ?;";

		MatchTags(Database &db, const char *sql)
		: SelectMultiple(db, sql)
		{ }

		void bind(const std::string_view &query, uint64_t limit, uint64_t offset) {
			bind_to(0, query);
			bind_to(1, limit);
			bind_to(2, offset);
		}
	};
};

