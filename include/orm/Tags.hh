#pragma once

#include <cassert>
#include <utility>
#include <cstddef>

#include "Statement.hh"
#include "Serializer.hh"
#include "Database.hh"
#include "UserError.hh"

class Tags
{
public:
	enum Field {
		TagId = 0,
		Name,
		Color,

		nr_fields
	};

	static const constexpr char *names[nr_fields] = {
		"tag_id",
		"name",
		"color"
	};

	template <typename T>
	inline static std::pair<const char *, T> make_pair(Field f, const T &value) {
		return std::make_pair(names[static_cast<size_t>(f)], value);
	}

	struct Create : public Statement {
		static const constexpr char *sql =
			"CREATE TABLE IF NOT EXISTS Tags ( "
				"tag_id INTEGER PRIMARY KEY, "
				"Name TEXT, "
				"Color TEXT "
			");";

		Create(Database &db)
		: Statement(db, sql)
		{ }
	};

	struct Insert : public Statement {
		static const constexpr char *sql =
			"INSERT INTO Tags (name, color) VALUES (?, ?);";

		Insert(Database &db)
		: Statement(db, sql)
		, m_db(db)
		{ }

		uint64_t tag_id = 0;
		std::string_view name;
		std::string_view color;

		void run() {
			if (name.empty())
				throw UserError(UserError::MissingFields);

			bind_to(0, name);
			bind_to(1, color);

			step();

			tag_id = m_db.get_rowid();
		}

		inline void write(Serializer &s) {
			assert(tag_id > 0);
			s.write_object(make_pair(TagId, tag_id));
		}

	private:
		Database &m_db;
	};

	struct Select : public Statement {
		static const constexpr char *sql =
			"SELECT tag_id, name, color FROM Tags;";

		Select(Database &db)
		: Statement(db, sql)
		{ }

		uint64_t tag_id = 0;
		std::string name;
		std::string color;

		inline bool run() { 
			if (!step())
				return false;

			load_at(0, tag_id);
			load_at(1, name);
			load_at(2, color);

			return true;
		}

		inline void write(Serializer &s) {
			s.write_object(
				make_pair(TagId, tag_id),
				make_pair(Name, name),
				make_pair(Color, color)
			);
		}
	};

	struct Delete : public Statement {
		static const constexpr char *sql =
			"DELETE FROM Tags WHERE tag_id = ?;";

		Delete(Database &db)
		: Statement(db, sql)
		, m_db(db)
		{ }

		uint64_t tag_id;

		inline void run() {
			assert(tag_id > 0);
			bind_to(0, tag_id);

			step();
		}

		inline void write(Serializer &s) {
			s.write_object();
		}

	private:
		Database &m_db;
	};
};


