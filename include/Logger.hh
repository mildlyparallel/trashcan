#pragma once

#include <fstream>
#include <iomanip>
#include <vector>
#include <tuple>
#include <cstdint>

class Logger
{
public:
	Logger();

	enum Level: size_t {
		Debug = 0,
		Verbose,
		Info,
		Warning,
		Error,

		nr_levels
	};

	template <typename... T>
	void error(T&&... args) const;

	template <typename... T>
	void warning(T&&... args) const;

	template <typename... T>
	void info(T&&... args) const;

	template <typename... T>
	void verbose(T&&... args) const;

	template <typename... T>
	void debug(T&&... args) const;

	Level get_level() const;
	void set_level(Level level);

	void set_output_file(const std::string &p);

protected:

private:
	template <typename... T>
	void log(Level level, T&&... args) const;

	static constexpr const char *m_level_str[nr_levels] = {
		"debug",
		"verbose",
		"info",
		"warning",
		"error",
	};

	Level m_level;

	std::ostream *m_out;

	std::ofstream m_file_stream;
};

extern Logger logger;

template <typename T>
std::ostream &print_container(std::ostream &out, const T &v) {
	out << "{";
	bool comma = false;

	const size_t max_el = 5;

	size_t sz = std::min(v.size(), max_el);
	for (size_t i = 0; i < sz; ++i) {
		if (comma)
			out << ", ";
		out << v[i];
		comma = true;
	}

	if (v.size() > max_el)
		out << ", ... (" << v.size() << ")";

	out << "}";
	return out;
}

template<typename... T, size_t... S>
std::ostream &print_tuple(
	std::ostream &out,
	const std::tuple<T...> &t,
	std::index_sequence<S...>
) {
	out << "(";

	int dummy[] = {
		(out << std::get<S>(t) << ((S + 1) == sizeof...(S) ? "" : ", "), 0)...
	};

	(void) dummy;

	out << ")";

	return out;
}

template <typename T>
std::ostream &operator <<(std::ostream &out, const std::vector<T> &v) {
	return print_container(out, v);
}

template <typename T, size_t N>
std::ostream &operator <<(std::ostream &out, const std::array<T, N> &v) {
	return print_container(out, v);
}

template <typename... T>
std::ostream &operator <<(std::ostream &out, const std::tuple<T...> &t) {
	return print_tuple(out, t, std::make_index_sequence<sizeof...(T)>());
}

template <typename... T>
void Logger::log(Logger::Level level, T&&... args) const
{
	if (level < m_level)
		return;

	// TODO use std::osyncstream when it's implementd

	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);

	std::stringstream ss;
	ss << "[" << std::put_time(&tm, "%T") << "]";
	ss << "[" << m_level_str[level] << "] ";

	(void) (int[]) { (ss << args << " ", 0)...  };

	*m_out << ss.str() << std::endl;
}

template <typename... T>
void Logger::error(T&&... args) const
{
	log(Error, args...);
}

template <typename... T>
void Logger::warning(T&&... args) const
{
	log(Warning, args...);
}

template <typename... T>
void Logger::info(T&&... args) const
{
	log(Info, args...);
}

template <typename... T>
void Logger::verbose(T&&... args) const
{
	log(Verbose, args...);
}

#ifndef NDEBUG
template <typename... T>
void Logger::debug(T&&... args) const
{
	log(Debug, args...);
}
#else
template <typename... T>
void Logger::debug(T&&...) const
{ }
#endif
