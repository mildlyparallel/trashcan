#pragma once

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define THROW_IF(cond) {\
	if ((cond)) \
		throw std::system_error(errno, std::generic_category(), __FILE__ ":" STR(__LINE__)); \
} while(false);

#include <string_view>
#include <string>
#include <algorithm>

namespace utils {

std::string first_word(const std::string_view &s);

template <typename A, typename B>
std::common_type_t<A, B> max(const A &a, const B &b)
{
	return std::max<std::common_type_t<A, B>>(a, b);
}

template <typename A, typename B>
std::common_type_t<A, B> min(const A &a, const B &b)
{
	return std::min<std::common_type_t<A, B>>(a, b);
}

} /* namespace utils */
