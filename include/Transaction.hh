#pragma once

class Database;

class Transaction
{
public:
	Transaction(Database &db);

	Transaction(const Transaction &other) = delete;

	Transaction(Transaction &&other) = delete;

	virtual ~Transaction();

	void commit();

private:
	Database &m_db;

	bool m_commited;
};

