#pragma once

#include <string_view>
#include <optional>
#include <iterator>
#include <tuple>
#include <ostream>
#include <filesystem>
#include <cstdint>

struct mg_connection;
struct mg_http_message;

class Request
{
public:
	enum Method : size_t {
		Get = 0,
		Post,
		Delete,
		Options,

		nr_methods
	};

	Request(mg_connection *con, mg_http_message *req);

	std::string_view method_str() const;
	std::string_view uri() const;
	std::string_view body() const;
	std::string_view query() const;
	Method method() const;

	bool match_uri(const char *uri) const;

	mg_connection *get_connection() const;

	mg_http_message *get_message() const;

	std::optional<std::string> get_query_var(const char *name) const;

	uint64_t get_query_ull(const char *name) const;

	std::string_view get_uri_var(size_t pos_from_end) const;

	size_t get_next_part(
		size_t offset,
		std::string_view &name,
		std::string_view &filename,
		std::string_view &body
	) const;

	void reply() const;

	void reply(const std::string &body) const;

	void reply(int code, const std::string &headers, const std::string &body) const;

	void serve_file(
		const std::filesystem::path &file,
		const std::string &mime,
		const std::string &hdrs
	) const;

	void serve_dir(const std::string &root_dir) const;

	void reply_error(int code, const char *msg) const;

	std::tuple<std::string_view, std::string_view> get_first_file() const;

	struct Iterator
	{
		public:
			Iterator(mg_http_message *msg, size_t offset);

			using iterator_category = std::input_iterator_tag;
			using value_type        = std::tuple<std::string_view, std::string_view, std::string_view>;
			using reference         = value_type&;

			Iterator &operator++();

			reference operator*();

			friend bool operator== (const Iterator& a, const Iterator& b);
			friend bool operator!= (const Iterator& a, const Iterator& b);

		private:
			mg_http_message *m_message;
			size_t m_offset;
			value_type m_value;
	};

	Iterator begin() const;
	Iterator end() const;

protected:
	Method m_method;

	static constexpr const char *methods_strings[nr_methods] = {
		"GET",
		"POST",
		"DELETE",
		"OPTIONS",
	};

private:
	friend std::ostream &operator<< (std::ostream &os, Request::Method method);

	void get_method_value();

	mg_connection *m_connection;

	mg_http_message *m_message;
};

std::ostream &operator<< (std::ostream &os, Request::Method method);

