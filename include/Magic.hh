#pragma once

#include <memory>
#include <string_view>

struct magic_set;

class Magic
{
public:
	Magic();

	virtual ~Magic();

	void load();

	const char *mime(const std::string_view &buf) const;

private:
	magic_set *m_cookie;
};
