#pragma once

#include <string>
#include <tuple>
#include <string_view>
#include <sstream>
#include <functional>
#include <iostream>
#include <cstdint>

struct sqlite3;

class Statement;

class Database
{
public:
	Database();

	virtual ~Database();

	void open(const std::string &p);

	void run_file(const std::string &p);

	void exec(const char *sql);

	uint64_t get_rowid() const;

	static bool is_threadsafe();

protected:

private:
	friend Statement;

	void init_pragmas();

	sqlite3 *get_handle();

	sqlite3 *m_handle;
};

class SqliteException: public std::exception {
public:
	SqliteException(int err_code);

	const char* what() const noexcept override;

private:
	int m_code;
};

