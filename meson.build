project(
	'trashcan',
	'cpp',
	default_options : [
		'buildtype=release',
		'cpp_std=gnu++2a'
	],
	version: '1.3.3'
)

cpp = meson.get_compiler('cpp')

datadir = get_option('datadir') / 'trashcan'

config = configuration_data()
config.set('version', meson.project_version())
config.set('TC_MAX_REQUEST_SIZE', get_option('max-request-size'))
config.set('TC_POLL_INTERVAL_MS', get_option('poll-interval-ms'))
config.set('TC_ALLOW_CORS', get_option('allow-cors'))
config.set_quoted('TC_API_PREFIX', get_option('api-prefix'))
config.set('datadir', get_option('prefix') / datadir)
config.set('storagedir', get_option('default-storage-dir'))

configure_file(
	input : 'config.h.in',
	output : 'config.h',
	configuration : config
)

trashcan_sources = [
	'src/Server.cc',
	'src/Config.cc',
	'src/Logger.cc',
	'src/TrashServer.cc',
	'src/Database.cc',
	'src/Statement.cc',
	'src/Transaction.cc',
	'src/Request.cc',
	'src/Search.cc',
	'src/Magic.cc',
	'src/Indexer.cc',
	'src/Utils.cc',
	'src/FileStorage.cc',
]

incdir = [
	include_directories('include')
]

deps = [  ]
deps += dependency('sqlite3', required : true)
deps += dependency('threads', required : true)
deps += cpp.find_library('magic', required : true)
dashh_dep = dependency('dashh', required : false)

if not dashh_dep.found()
	dashh_subporject = subproject('dashh',
		default_options : [
			'build-as-subproject=true',
		]
	)
	dashh_dep = dashh_subporject.get_variable('dashh_dep')
endif

deps += dashh_dep

mg_subproject = subproject('mongoose',
	default_options : [
		'max-recv-buf-size=' + get_option('max-request-size').to_string(),
		'max-recv-buf-growth=' + get_option('max-recv-buf-growth').to_string()
	]
)

deps += mg_subproject.get_variable('mg_dep')

debug_mode = get_option('buildtype').startswith('debug')

cxxflags = [ ]

if debug_mode
	message('Builing in debug mode.')
	cxxflags += '-Wall'
	cxxflags += '-Wextra'
else
	cxxflags += '-DNDEBUG=1'
endif

trashcan_library = static_library(
	'trashcan',
	sources : trashcan_sources,
	include_directories : incdir,
	cpp_args : cxxflags,
	dependencies : deps,
	install : false
)

executable(
	'trashcan',
	sources : 'src/trashcan.cc',
	include_directories : incdir,
	link_with : trashcan_library,
	cpp_args : cxxflags,
	dependencies : deps,
	install: true
)

install_subdir('frontend/dist', install_dir : datadir)
install_data('scripts/extract.sh', install_dir : datadir)

# if debug_mode
# 	subdir('tests')
# endif

